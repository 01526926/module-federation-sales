export interface Order {
  id: number;
  articleId: number;
  quantity: number;
  completed: boolean;
}
