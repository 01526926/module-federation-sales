import {Routes} from '@angular/router';
import {SalesComponent} from './sales/sales.component';

export const ARTICLE_ROUTES: Routes = [
  {
    path: '',
    component: SalesComponent
  }
]
