import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {OrderService} from '../service/order.service';
import {Order} from '../model/order';

@Component({
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {
  orders: Order[] = [];
  activeOrder: Order = {id: 0, articleId: 0, quantity: 1, completed: false};
  form: any;

  constructor(private formBuilder: FormBuilder, private orderService: OrderService) {
  }

  ngOnInit(): void {
    this.orderService.getOrders().subscribe(o => {
      this.orders = o;
      this.activeOrder = this.orders[0];
    });
    this.form = this.formBuilder.group({
      id: [0],
      articleId: [0],
      quantity: [0],
      completed: [false]
    });
  }

  onRowClick(rowIndex: number) {
    this.setActive(rowIndex);
  }

  onSave() {
    alert('Save data was clicked.');
  }

  private setActive(rowIndex: number) {
    this.activeOrder = this.orders[rowIndex];
  }
}
