import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SalesComponent} from './sales/sales.component';
import {RouterModule} from '@angular/router';
import {ARTICLE_ROUTES} from './sales.routes';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [SalesComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    RouterModule.forChild(ARTICLE_ROUTES)
  ]
})
export class SalesModule {
}
