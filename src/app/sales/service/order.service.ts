import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Order} from '../model/order';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  orders = [
    {
      id: 1,
      articleId: 77857,
      quantity: 5,
      completed: true
    },
    {
      id: 2,
      articleId: 55,
      quantity: 56,
      completed: false
    }
  ];

  private static readonly ORDER_ENDPOINT = environment.apiUrl + '/order';

  constructor(/* private http: HttpClient */) {
  }

  getOrders(): Observable<Order[]> {
    return of(this.orders);
    // // This does not work currently due to HttpClient injection error
    // return this.http.get<Order[]>(ArticleService.ORDER_ENDPOINT);
  }
}
